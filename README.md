## Index
1. [Intro](#id-intro)
1. [Bàsics](#id-basics)
2. [JSON](#id-json)
3. [MySQL](#id-mysql)

<div id='id-intro'/>

## Intro

En aquesta pràctica implementarem tant el servidor com el client d'un chat. Per tal d'aconseguir-ho utilitzarem el següent:

* **Servidor**: Serà implementat en javascript utilitzant [*node.js*](https://nodejs.org/). Ens ajudarem del modul *mysql* per tal de gestionar la base de dades. *Node* serà l'encarregat d'executar la part servidor. 

* **Client**: El client serà implementat utilitzant la tecnologia html/javascript/css i executada als navegadors. 

<div id='id-basics'/>

## Bàsics

Aquí podeu trobar l'esquelet que farem servir per la pràctica del Chat. 

L'estructura de carpetes que usarem és el següent:

* **Client** Aquí posarem tots els fitxers del client
    * ***index.html*** 
    * **js** En aquesta carpeta hi posarem els fitxers de codi javascript de la banda client 
        * ***client.js*** Fitxer javascript principal del client
    * **css** Hi posarem tots els fitxers css
        * ***styles.css*** El fitxer css principal
* **Server** Aquí hi posarem tot els fitxers de codi referent al servidor
    * ***main.js*** Fitxer "main" del servidor
    * ***ChatServer.js*** Codi de la classe principal del servidor *ChatServer*

Per tal d'executar el Servidor necessitarem *node.js*. 
Un cop tinguem instal·lat el node.js, ens situarem a la carpeta **Servidor** i procedirem a instal·lar el modul websocket:

```npm install websocket```  

Un cop instal·lats tots els prerequisits, podem executar el codi del servidor amb la següent comanda:

```node main.js```

<div id='id-json'/>

## JSON
* Per tal de convertir un objecte de javascript a un string (json):

    ``` JSON.stringify( objecte ); ```

    Per exemple:

    ```
    var obj = {
        foo: 'hola',
        boo: 3
    };
    var obj_string = JSON.stringify( obj );
    console.log(obj_string);

    ```
    
    Imprimirà

    ```
    {"foo":"hola","boo":3}
    ```
* Per tal de fer l'operació inversa, convertir un string (en json) a un objecte de javascript:

    ```
    var str = '{"foo":"hola","boo":3}';
    var obj = JSON.parse(str);
    ```

<div id='id-mysql'/>

## MySQL 

1. Primer necessitem tenir instal·lat el modul de node *mysql*. Per aconseguir-ho ens situarem a la carpeta Server i executarem:

    ``` npm install mysql ```

2. Un cop instal·lat, importarem el modul:

    ``` var mysql = require('mysql'); ```

3. Connectar a una base de dades mysql:

    ```
    var con = mysql.createConnection({
        host: "localhost",
        user: "yourusername",
        database: "database",
        password: "yourpassword"
    });

    con.connect(function(err) {
        if (err) throw err;
        console.log("Connectat!");
    });

    ```

4. Un cop s'ha establert la connexió podem enviar consultes:

    ```
    con.query(sql, function (err, result) {
        if (err) throw err;
        console.log("Result: " + result);
    });
    ```

    On *sql* és una sentencia en SQL.